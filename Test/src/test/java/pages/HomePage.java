package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by pc on 28-Jan-17.
 */
public class HomePage {
    private WebDriver driver;

    public HomePage(WebDriver driver){
        this.driver = driver;
        initializePage();
    }
    public List<WebElement> getMenuItems()
    {
        return driver.findElements(By.className("sf-with-ul"));
    }
    @FindBy(how = How.XPATH,using = ".//*[@id='block_top_menu']/ul/li")
    public List<WebElement> menuItems;

    public void initializePage(){
        PageFactory.initElements(driver,this);
    }
    public void clickOnMenuIndex(int index){
        menuItems.get(index).click();
    }
}
