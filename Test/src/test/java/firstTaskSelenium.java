import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.Collection;

import static java.lang.Thread.*;

/**
 * Created by pc on 28-Jan-17.
 */
@RunWith(Parameterized.class)
public class firstTaskSelenium {
    private static WebDriver driver;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {2,1}
        });
        //, { 1, 1 }, { 2, 1 }, { 3, 2 }, { 4, 3 }, { 5, 5 }, { 6, 8 }
    }
    private Integer numberOfProductsToBuy;
    private Integer index;
    public firstTaskSelenium(Integer numberOfProductsToBuy, Integer index) {
        this.numberOfProductsToBuy = numberOfProductsToBuy;
        this.index = index;
    }
    @BeforeClass
    public static void BeforeClass() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\pc\\Desktop\\Selenium1\\Test\\$(project.basedi)\\src\\test\\java\\resources\\standalone\\windows\\googlechrome\\64bit\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void addingOneProductShouldBeProperlyDisplayInCart() throws InterruptedException {

        Integer number_of_products = numberOfProductsToBuy;
        driver.get("https://testingcup.pgs-soft.com/task_1");
        WebElement price_section = driver.findElements(By.className("caption")).get(index);

        String price_text = price_section.getText();
        String price_tag = "Cena:";
        String price = price_text.substring(price_text.indexOf(price_tag)+price_tag.length(), price_text.indexOf("zł")).trim();


        Float parsed_price = Float.parseFloat(price);

        WebElement input = driver.findElements(By.className("form-control")).get(index);
        input.clear();
        input.sendKeys(number_of_products.toString());
        WebElement button = driver.findElements(By.cssSelector("button[id*='add-product']")).get(index);
        button.click();

        WebElement price_summary = driver.findElement(By.className("summary-price"));

        Assert.assertFalse(price_summary.getText().equals("0.00 zł"));

        Float expected_summary_price = (parsed_price * number_of_products);
        String expected_summary_prie_estring = String.format("%.2f", expected_summary_price) ;
        System.out.print(expected_summary_prie_estring + "\n");
        System.out.print(price_summary.getText());
        Assert.assertTrue(price_summary.getText().equals(expected_summary_prie_estring + " zł"));

        //page refresh is too slow
        driver.navigate().refresh();
        WebDriverWait web_wait = new WebDriverWait(driver,1000);

    }

    @AfterClass
    public static void AfterClass() {

        driver.quit();
    }
}
