import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by pc on 28-Jan-17.
 */
public class firstSeleniumTest {
    private  static WebDriver driver;
    @BeforeClass
    public static void BeforeClass()
    {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\pc\\Desktop\\Selenium1\\Test\\$(project.basedi)\\src\\test\\java\\resources\\standalone\\windows\\googlechrome\\64bit\\chromedriver.exe");
        driver= new ChromeDriver();
    }
    @Test
    public void firstTest()
    {
        driver.manage().window().maximize();
        driver.get("https://asta.pgs-soft.com/");
        String page_title = driver.getTitle();
        System.out.print(page_title);
        //System.out.print(driver.getPageSource());

        Assert.assertTrue(page_title.equals("Automated Software Testing Arena"));
        Assert.assertTrue(driver.getPageSource().contains("Buggy"));
    }
    @Test
    public void firstSelectorById() {
        driver.get("https://asta.pgs-soft.com/");
        WebElement web_element = driver.findElement(By.id("name"));
        System.out.print(web_element.getLocation());
    }
    @Test
    public void findElementByTagName() {
        driver.get("https://asta.pgs-soft.com/");
        WebElement web_element = driver.findElement(By.tagName("body"));
        System.out.print(web_element.getLocation());
    }
    @Test
    public void findElementByClassName() {
        driver.get("https://asta.pgs-soft.com/");
        WebElement web_element = driver.findElement(By.className("Logo-asta"));
        System.out.print(web_element.getLocation());
    }
    @Test
    public void findElementByCssSelector() {
        driver.get("https://asta.pgs-soft.com/");
        WebElement web_element = driver.findElement(By.cssSelector("Logo-asta"));
        System.out.print(web_element.getLocation());
    }
    @AfterClass
    public static void AfterClass(){

        driver.quit();
    }
}
