import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.Arrays;


/**
 * Created by pc on 28-Jan-17.
 */
public class FirstTest {

    @Before
    public void Before(){
        System.out.print("before");
    }
    @Test
    public void firstTest()
    {
        String something = "sdada s ad asd a";
        Assert.assertTrue(something.contains("a"));
    }
    @Test
    public void intTest()
    {
        Integer[] something = {1,23,3,23,5,23,2,4,};
        Assert.assertTrue(Arrays.asList(something).contains(5));
    }
    @After
    public void After(){
        System.out.print("after");
    }
}
