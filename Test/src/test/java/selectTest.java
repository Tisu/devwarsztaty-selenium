import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.HomePage;
import pages.ProductPage;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by pc on 28-Jan-17.
 */
public class selectTest {
    private static WebDriver driver;

    @BeforeClass
    public static void BeforeClass() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\pc\\Desktop\\Selenium1\\Test\\$(project.basedi)\\src\\test\\java\\resources\\standalone\\windows\\googlechrome\\64bit\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void sortByAZ() throws InterruptedException {

        driver.get("http://automationpractice.com/");

        HomePage home_page = new HomePage(driver);
        home_page.clickOnMenuIndex(1);

        ProductPage product_page = new ProductPage(driver);
        List<String> products_before_sort = product_page.getProductsNameFromProductList();

        product_page.sortByValue("name:asc");
        WebDriverWait web_wait = new WebDriverWait(driver,2000);
        web_wait.until(ExpectedConditions.invisibilityOfAllElements(product_page.getProductNamesElementsFromProductList()));
        List<String> products_after_sort_call = product_page.getProductsNameFromProductList();

        Collections.sort(products_before_sort);


        for (int i =0; i< products_before_sort.size();i++){
            Assert.assertTrue(products_before_sort.get(i).equals( products_after_sort_call.get(i)));
        }
    }

    @AfterClass
    public static void AfterClass() {

        driver.quit();
    }
}
