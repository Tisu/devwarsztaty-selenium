import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by pc on 28-Jan-17.
 */
public class actionTest {
    private static WebDriver driver;

    @BeforeClass
    public static void BeforeClass() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\pc\\Desktop\\Selenium1\\Test\\$(project.basedi)\\src\\test\\java\\resources\\standalone\\windows\\googlechrome\\64bit\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void rightClickAndChangeNameShouldBeProperlyDisplayed() throws InterruptedException {

        driver.get("https://testingcup.pgs-soft.com/task_9");

        // WebDriverWait web_wait = new WebDriverWait(driver,1000);
        WebElement element = driver.findElement(By.id("j1_1_anchor"));
        Actions action = new Actions(driver);

        action.contextClick(element).build().perform();
        WebDriverWait web_wait = new WebDriverWait(driver, 2000);

        WebElement click_element = driver.findElement(By.className("vakata-context-hover"));
        web_wait.until(ExpectedConditions.visibilityOf(click_element));
        click_element.click();

        WebElement input = driver.findElement(By.className("jstree-rename-input"));
        web_wait.until(ExpectedConditions.visibilityOf(input));
        input.clear();
        String renamed_folder = "sdadadasdaęęęęsd!@32";
        input.sendKeys(renamed_folder);
        input.sendKeys(Keys.ENTER);

        WebElement expected_change = driver.findElement(By.className("col-md-12")).findElement(By.tagName("h1"));
        System.out.print(expected_change.getText());
        //Assert.assertEquals(expected_change,(renamed_folder));
    }

    @AfterClass
    public static void AfterClass() {

        driver.quit();
    }
}
