package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 28-Jan-17.
 */
public class ProductPage {
    private WebDriver driver;

    public ProductPage(WebDriver driver) {
        this.driver = driver;
        initializePageElements();
    }
    @FindBy(how = How.ID,using = "selectProductSort")
    public List<WebElement> productNames;

    @FindBy(how = How.ID,using = "center_column")
    public WebElement productContainer;
    @FindBy(how = How.ID,using = "selectProductSort")
    public WebElement selectContainer;

    public void initializePageElements(){
        PageFactory.initElements(driver,this);
    }
    public void sortByValue(String value){
        Select select_elenment = new Select(selectContainer);
        select_elenment.selectByValue(value);
    }
    public List<WebElement> getProductNamesElementsFromProductList(){
        return productContainer.findElements(By.className("product-name"));
    }

    public List<String> getProductsNameFromProductList(){
        List<WebElement> product_list = getProductNamesElementsFromProductList();
        List<String> names = new ArrayList<String>();
        for (WebElement item: product_list){
            names.add(item.getText());
        }
        return names;
    }
}
